from setuptools import setup

def readme():
	with open('README.md') as f:
		return f.read()

setup(name='mathboilerplate',
	  version='0.1',
	  description='A properly built package for mathematics',
	  classifiers=[
			  'Topic :: Scientific/Engineering',
			  'License :: OSI Approved :: MIT License',
			  'Programming Language :: Python :: 2.7',
			  ],
	  keywords='math science boilerplate',
	  url='https://gitlab.com/tomboulier/mathboilerplate',
	  author='Thomas Boulier',
	  author_email='thomas.boulier@polytechnique.edu',
	  license='MIT',
	  packages=['mathboilerplate'],
	  test_suite='nose.collector',
	  tests_require=['nose'],
	  install_requires=[
			  'numpy',
			  ],
	  zip_safe=False)
