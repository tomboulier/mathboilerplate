Welcome to Math Boilerplate
===========================

This package is intended to show how to properly build a mathematics-oriented package using Python. It implements very simple mathematics, but uses `software engineering best practices <http://software-carpentry.org/blog/2014/01/best-practices-has-been-published.html>`_:

- `version control using Git <http://swcarpentry.github.io/git-novice/>`_,

- `proper Python packaging structure <http://python-packaging.readthedocs.io>`_,

- documentation using `Sphinx <http://www.sphinx-doc.org/>`_,

- testing using `py.test <http://pytest.org/>`_ (and `doctests <https://docs.python.org/2.7/library/doctest.html>`_).

Installation
------------

Open a terminal and type::
   
   $ git clone https://gitlab.com/tomboulier/mathboilerplate
   $ cd mathboilerplate
   $ make

This will install the package, run the tests, and compile the documentation.

If you just want to install the package, you can simply type::

	$ sudo python setup.py install

To run the tests, the command is::

	$ py.test

To compile the documentation, use the following command::

	$ sudo make doc

Usage
-----

Once installed, you can use the (only) function of the module, `sum`. Open a Python terminal and run the following commands::

	>>> from mathboilerplate import sum
	>>> sum(1,2)
	3