mathboilerplate package
=======================

.. automodule:: mathboilerplate
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    mathboilerplate.tests

Submodules
----------

mathboilerplate.simplesum module
--------------------------------

.. automodule:: mathboilerplate.simplesum
    :members:
    :undoc-members:
    :show-inheritance:


