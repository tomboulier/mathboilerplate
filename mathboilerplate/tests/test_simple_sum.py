"""
Unit testing is performed in a separate module, which improves readability.
"""

from unittest import TestCase

import mathboilerplate as mb

class TestMathUnit(TestCase):
	def test_1_1_gives_2(self):
		"""The test presented here is the dumbest possible: :math:`1+1=2`."""
		self.assertTrue(mb.sum(1,1)==2)

	def test_float_numbers(self):
		"""You wouldn't rely on a single addition, so you'd test another
		(slightly) more complicated test: here :math:`3.2+4.0=7.2`."""
		self.assertTrue(mb.sum(3.2,4.0)==7.2)

class TestMathFormula(TestCase):
	def test_formula_sum_of_N_integers(self):
		"""
		Sometimes, you want to test more difficult formulas, like this one:

		.. math::

			\sum_{k=1}^n k = \\frac{n(n+1)}{2}
		"""
		N = 100
		res = 0
		for k in range(N):
			res = mb.sum(k+1,res)

		self.assertTrue( res == N*(N+1)/2 )

