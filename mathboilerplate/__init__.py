"""
This package is short, but it has everything you need:

- automatic build,

- automatic testing,

- automatic documentation.

We can have some math in the documentation, from inline math like :math:`a^2 +
b^2 = c^2`, to display math like the following

.. math::

	\sum_{n=1}^{\infty} \\frac{1}{n^2} = \\frac{\pi^2}{6}.

"""

from .simplesum import sum
