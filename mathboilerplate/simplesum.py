"""
As you can see, this module is quite minimalist: it computes the sum of two
numbers x and y.
"""

import numpy as np

def sum(x,y):
    """Adds two numbers and returns the result.
 
        :param x: The first number to add
        :param y: The second number to add
        :type x: float
        :type y: float
        :return: The result of the addition
        :rtype: float
 
        :Example:
 
        >>> sum(1, 1)
        2
        >>> sum(3.2, 4.3) 
        7.5
    """
    return x+y

# These lines will activate automatic testing of docstrings
if __name__ == "__main__":
    import doctest
    doctest.testmod()
